package com.nghiacoder.respository;

import com.nghiacoder.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer>{


}
