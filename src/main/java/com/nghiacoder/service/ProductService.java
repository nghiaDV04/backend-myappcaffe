package com.nghiacoder.service;

import java.util.List;

import com.nghiacoder.model.Product;

import org.springframework.stereotype.Service;

@Service
public interface ProductService {

	public Product createProduct(Product product);
	
	public List<Product> getAllProduct();
	
	public Product getProductById(int id);
	
	public void deleteProduct(int id);
	
	public Product update(int id, Product product);
	
}
