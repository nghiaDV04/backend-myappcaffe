//package com.nghiacoder.service;
//
//import com.nghiacoder.model.Employee;
//import com.nghiacoder.model.EmployeeDTO;
//import com.nghiacoder.respository.EmployeeRepository;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.password.PasswordEncoder;
//
//public class EmployeeServiceImpl implements EmployeeService{
//	
//	@Autowired
//	private EmployeeRepository employeeRepo;
//	
//	@Autowired
//	private PasswordEncoder passwordEncoder;
//	
//	@Override
//	public String addEmployee(EmployeeDTO employeeDTO) {
//		
//		Employee employee = new Employee(
//				employeeDTO.getEmployeeid(),
//				employeeDTO.getEmployeename(),
//				employeeDTO.getEmail(),
//				this.passwordEncoder.encode(employeeDTO.getPassword())
//				);
//		
//			employeeRepo.save(employee);
//		return employee.getEmployeename();
//	}
//
//}
