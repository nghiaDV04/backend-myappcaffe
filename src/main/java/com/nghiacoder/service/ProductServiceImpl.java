package com.nghiacoder.service;

import java.util.List;

import com.nghiacoder.model.Product;
import com.nghiacoder.respository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl  implements ProductService{

	@Autowired
	private ProductRepository proRepo;

	@Override
	public Product createProduct(Product product) {	
		return proRepo.save(product);
	}

	@Override
	public List<Product> getAllProduct() {
		
		return proRepo.findAll();
	}

	@Override
	public Product getProductById(int id) {
		
		return proRepo.findById(id).get();
	}

	@Override
	public void deleteProduct(int id) {
		Product product = proRepo.findById(id).get();
		if (product != null) {
			proRepo.delete(product);
		}
		
	}

	@Override
	public Product update(int id, Product product) {
		Product oldproduct = proRepo.findById(id).get();
		if (oldproduct != null) {
			product.setId(id);
			return proRepo.save(product);
		}
		return null;
	}
	
	
}
