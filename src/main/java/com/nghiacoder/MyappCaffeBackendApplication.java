package com.nghiacoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyappCaffeBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyappCaffeBackendApplication.class, args);
	}

}
