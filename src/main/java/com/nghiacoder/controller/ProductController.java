package com.nghiacoder.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.nghiacoder.model.Product;
import com.nghiacoder.model.ProductDTO;
import com.nghiacoder.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@PostMapping("/save")
	public ResponseEntity<Product> createProduct(@RequestBody Product product){
		return new ResponseEntity<Product>(productService.createProduct(product), HttpStatus.CREATED);
	}
	
//	@GetMapping("/")
//	public ResponseEntity<List<Product>> getAllProduct(){
//		return new ResponseEntity<List<Product>>(productService.getAllProduct(),HttpStatus.OK);
//	}
	
	@GetMapping("/")
	  public ResponseEntity<List<ProductDTO>> getProducts(){
	    List<Product> products = productService.getAllProduct();
	    List<ProductDTO> productDTOs = products.stream().map(ProductDTO::new).collect(Collectors.toList());
	    return ResponseEntity.ok(productDTOs);
	  }
	
	@GetMapping("{id}")
	public ResponseEntity<Product> getEmpById(@PathVariable int id) {
		return new ResponseEntity<Product>(productService.getProductById(id), HttpStatus.OK);
	}

	@GetMapping("/delete/{id}")
	public ResponseEntity<String> deleteEmp(@PathVariable int id) {
		productService.deleteProduct(id);
		return new ResponseEntity<String>("Delete thành công", HttpStatus.OK);
	}

	@PostMapping("/update/{id}")
	public ResponseEntity<Product> updateEmp(@PathVariable int id, @RequestBody Product product) {

		return new ResponseEntity<Product>(productService.update(id, product), HttpStatus.OK);
	}
	
}
